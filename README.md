# Overview

This is a sample student repository that supports automated testing.

# Notes

## Preserving output artifacts

Most "generated" files are deleted after each run. However, the following file patterns are
preserved, and may be viewed from the same screen that students can view the output of running
the test scripts.

* Minimum requirements tests: `minimum/*.html` and `minimum/*.txt`
* Functional tests: `functional/*.html` and `functional/*.txt`

If your test script (specified in a [file like `assignment-testing.toml`](https://git.ucsc.edu/teaching-utils/class-testing-utils) )
writes output files that match any of the above patterns to the testing directory, the artifacts
will be preserved. All other generated files will be discarded after the test run.
